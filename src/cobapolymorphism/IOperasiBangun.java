package cobapolymorphism;

public interface IOperasiBangun {
    
    int hitungLuas();
    int hitungKeliling();
    
}
