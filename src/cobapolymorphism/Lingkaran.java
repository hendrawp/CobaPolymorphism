package cobapolymorphism;

public class Lingkaran implements IOperasiBangun{

    @Override
    public int hitungLuas() {
        return 8;
    }

    @Override
    public int hitungKeliling() {
        return 8;
    }
    
}
