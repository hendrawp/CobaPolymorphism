package cobapolymorphism;

public class Persegi extends BangunDatar implements IOperasiBangun {
 
    @Override
    String getNamaBangun() { return "Persegi"; }
    
    int keliling (int sisi) {return 0; }

    @Override
    public int hitungLuas() {
        return 5;
    }

    @Override
    public int hitungKeliling() {
        return 5;
    }
    
}
