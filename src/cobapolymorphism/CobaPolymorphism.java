package cobapolymorphism;

public class CobaPolymorphism {

    public static void main(String[] args) {
        
        BangunDatar b = new BangunDatar();
        System.out.println(b.getNamaBangun());
        
        BangunDatar s = new Segitiga();
        System.out.println(s.getNamaBangun());
        
        BangunDatar p = new Persegi();
        System.out.println(p.getNamaBangun());
        
        IOperasiBangun x = new Persegi();
        System.out.println(x.hitungLuas());
        
        IOperasiBangun y = new Lingkaran();
        System.out.println(y.hitungLuas());
        
        Persegi segi = new Persegi();
        tampilNama(segi);
        tampilNama(new Segitiga());
        tampilLuas(segi);
        tampilLuas(new Lingkaran());
        
        System.out.println(p instanceof Persegi);
        System.out.println(p instanceof BangunDatar);
        System.out.println(s instanceof Persegi);
        System.out.println(s instanceof BangunDatar);
        System.out.println(p instanceof IOperasiBangun);
        
        Persegi persegi = (Persegi) p;
        persegi.keliling(0);
        IOperasiBangun iop = (IOperasiBangun) p;
        iop.hitungKeliling();
        
    }
    
    static void tampilNama(BangunDatar x) {
        System.out.println("Nama Bangun : "+x.getNamaBangun());
    }
    
    static void tampilLuas(IOperasiBangun x) {
        System.out.println("Luas : "+x.hitungLuas());
    }
    
}
